package com.srock.catget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by Pawel on 2015-11-09.
 */
public class AlarmUtilis {

    private static final int REFRESH_ALARM_ID = 0;

    /**
     * This method starts alarm for automatic update of the widget
     * depending on the time chosen by the user.
     * @param context
     */
    public static void startAlarm(Context context) {
        Log.i("Alarmstarted", "Alarmstarted");

        PendingIntent catFactRefreshPendingIntent = PendingIntent.getBroadcast(context, REFRESH_ALARM_ID,
                FactsSettings.getRefreshIntent(context), PendingIntent.FLAG_UPDATE_CURRENT);

        int intervalMillis = (int) FactsSettings.getRefreshTime(context);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MILLISECOND, intervalMillis);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), intervalMillis, catFactRefreshPendingIntent);
    }

    public static void stopAlarm(Context context) {
        Log.i("Alarstoped", "Alarmstoped");
        PendingIntent catFactRefreshPendingIntent = PendingIntent.getBroadcast(context, REFRESH_ALARM_ID,
                FactsSettings.getRefreshIntent(context), PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(catFactRefreshPendingIntent);
    }


}
