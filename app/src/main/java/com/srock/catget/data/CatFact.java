package com.srock.catget.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pawel on 2015-11-02.
 */


public class CatFact implements Parcelable {
    private String id;
    private String fact;

    /**
     *
     * @param fact String that contains the fact shown on the widget
     * @param id String that contains ID of the fact shown on the widget
     */
    public CatFact(String fact, String id) {
        this.fact = fact;
        this.id = id;
    }

    public String getFact() {
        return fact;
    }

    public String getId() {
        return id;
    }

    public void setFact(String fact) {
        this.fact = fact;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.fact);
    }

    protected CatFact(Parcel in) {
        this.id = in.readString();
        this.fact = in.readString();
    }

    public static final Parcelable.Creator<CatFact> CREATOR = new Parcelable.Creator<CatFact>() {
        public CatFact createFromParcel(Parcel source) {
            return new CatFact(source);
        }

        public CatFact[] newArray(int size) {
            return new CatFact[size];
        }
    };
}
