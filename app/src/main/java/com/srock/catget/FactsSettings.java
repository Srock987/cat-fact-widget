package com.srock.catget;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.format.DateUtils;

import com.srock.catget.broadcastreceivers.AlarmBroadcastReceiver;

/**
 * Created by Pawel on 2015-11-03.
 */
public class FactsSettings {

    public static final String FACT_REFRESH_TIME_VALUE = "FACT_REFRESH_TIME_VALUE";

    private static final long DEFAULT_REFRESH_TIME = DateUtils.DAY_IN_MILLIS;

    public static final String NOTIFICATION_STATUS = "NOTIFICATION_STATUS";

    public static final boolean DEFAULT_NOTIFICATION_STATUS = true;

    public static final String REFRESH_BUTTON_STATUS = "REFRESH_BUTTON_STATUS";

    public static final boolean DEFAULT_REFRESH_BUTTON_STATUS = false;

    /**
     * This method returns refresh time value from sharedPreferences
     * @param context
     * @return long refresh time value
     */
    public static long getRefreshTime(Context context) {
        return getSharedPreferences(context).getLong(FACT_REFRESH_TIME_VALUE, DEFAULT_REFRESH_TIME);
    }

    /**
     * This method change refresh time from millis to hours and minutes depending
     * on refresh time chosen by the user
     * @param context context
     * @return String refresh time in certain format
     */
    public static String getRefreshTimeForPreferenceSummary(Context context) {
        long TimeInMillis = getRefreshTime(context);
        long TimeInMinutes = TimeInMillis / 60000;
        long TimeInHours = TimeInMinutes / 60;
        if (TimeInHours < 1) {
            return "Every " + String.valueOf(TimeInMinutes) + " minutes.";
        }
        if (TimeInHours == 1) {
            return "Every hour.";
        } else
            return "Every " + String.valueOf(TimeInHours) + " hours.";
    }

    /**
     * This method change refresh time value in sharedPrefences
     * to to another value specified by the user
     * @param context context
     * @param specificRefreshTime refresh time set by user
     */
    public static void setRefreshTime(Context context, long specificRefreshTime) {
        SharedPreferences.Editor editor = getEdit(context);
        editor.putLong(FACT_REFRESH_TIME_VALUE, specificRefreshTime);
        editor.apply();
        AlarmUtilis.startAlarm(context);
    }

    /**
     * This method returns notification status in shared preferences.
     * @param context context
     * @return notification status
     */
    public static boolean getShowNotificationStatus(Context context) {
        return getSharedPreferences(context).getBoolean(NOTIFICATION_STATUS, DEFAULT_NOTIFICATION_STATUS);
    }

    /**
     * This method sets notification status in shared preferences
     * depending on the user choice.
     * @param context context
     * @param specificNotificationStatus notification status specified by the user
     */
    public static void setNotificationStatus(Context context, boolean specificNotificationStatus) {
        SharedPreferences.Editor editor = getEdit(context);
        editor.putBoolean(NOTIFICATION_STATUS, specificNotificationStatus);
        editor.apply();
    }

    /**
     * This method returns refresh button status from shared preferences
     * @param context context
     * @return refresh button status
     */
    public static boolean getRefreshButtonStatus(Context context) {
        return getSharedPreferences(context).getBoolean(REFRESH_BUTTON_STATUS, DEFAULT_REFRESH_BUTTON_STATUS);
    }

    /**
     * This method sets refresh button status in shared preferences
     * to the value specified by the user
     * @param context context
     * @param specificRefreshButtonStatus specified refresh button status
     */
    public static void setRefreshButtonStatus(Context context, boolean specificRefreshButtonStatus) {
        SharedPreferences.Editor editor = getEdit(context);
        editor.putBoolean(REFRESH_BUTTON_STATUS, specificRefreshButtonStatus);
        editor.apply();
    }

    /**
     *
     * @param context
     * @return
     */
    private static SharedPreferences.Editor getEdit(Context context) {
        return getSharedPreferences(context).edit();
    }

    /**
     * This method returns default shared preferences.
     * @param context
     * @return default shared preferences
     */
    private static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * This method returns intent that calls the broadcast
     * to update widget
     * @param context
     * @return intent for updating widget
     */
    public static Intent getRefreshIntent(Context context) {
        Intent intent = new Intent(context, AlarmBroadcastReceiver.class);
        return intent;
    }
}
