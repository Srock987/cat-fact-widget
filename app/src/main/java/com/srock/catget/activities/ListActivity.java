package com.srock.catget.activities;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;


import com.srock.catget.R;
import com.srock.catget.database.FactContentProvider;
import com.srock.catget.database.FactTable;
import com.srock.catget.widget.FactListViewAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pawel on 2015-12-17.
 */
public class ListActivity extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_layout);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT,"ListActivity started.");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT,bundle);

        //creating listView and attaching it to layout
        final ListView listView = (ListView) findViewById(R.id.listView);

        //setting up cursor get form the sqlDatabase in order which has the highest value in COLUMN_SHOW_ORDER
        // (has been shown last on the widget) and COLUMN_SHOW_ORDER is not null.
        Cursor cursor = this.getContentResolver().query(FactContentProvider.CONTENT_URI, null,
                FactTable.COLUMN_SHOW_ORDER + " IS NOT NULL", null, FactTable.COLUMN_SHOW_ORDER + " DESC");
        //creating a list of string and filling positions by using method extractFactsFromCursor()
        List<String> factList = extractFactsFromCursor(cursor);

        //creating a new adapter with list of strings
        FactListViewAdapter adapter = new FactListViewAdapter(this, 0, factList);
        //attaching the adapter to listView
        listView.setAdapter(adapter);

        //creating toolbar and attaching it to layout
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //adding icon to toolbar
        getSupportActionBar().setIcon(R.drawable.ic_stat_ic_notification_icon);
        //adding title to toolbar
        getSupportActionBar().setTitle("Cat facts shown so far:");

        //creating the add and attaching it to layout
        AdView adView = (AdView) this.findViewById(R.id.adView);
        //building the add
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);//loading the add
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //inflating the menu
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.share_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //adding the setting option in menu for item
        // and giving it intent to start FactSettingActivity
        Intent intentSettings = new Intent(this, FactsSettingActivity.class);
        startActivityForResult(intentSettings, 0);
        return super.onOptionsItemSelected(item);
    }

    private List<String> extractFactsFromCursor(Cursor cursor) {
        //creating list of strings
        List<String> factList = new ArrayList<>();
        //filling the list with the facts for me cursor
        while (cursor.moveToNext()) {
            factList.add(cursor.getString(cursor.getColumnIndex(FactTable.COLUMN_FACT)));
        }
        return factList;
    }


}


