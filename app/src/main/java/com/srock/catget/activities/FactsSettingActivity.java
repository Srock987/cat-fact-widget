package com.srock.catget.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;

import com.srock.catget.FactsSettings;
import com.srock.catget.AlarmUtilis;
import com.srock.catget.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by Pawel on 2015-11-03.
 */
public class FactsSettingActivity extends Activity {


    private PrefsFragment mPreferenceFragment;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT,"SettingsActivity started.");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT,bundle);

        mPreferenceFragment = new PrefsFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.container_fragment, mPreferenceFragment).commit();

        AdView adView = (AdView) this.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("AE7879F6E2F48700CD6A65730D14581E")
                .build();
        adView.loadAd(adRequest);
    }

    public static class PrefsFragment extends PreferenceFragment implements
            SharedPreferences.OnSharedPreferenceChangeListener, Preference
            .OnPreferenceChangeListener {

        public static final String FACT_REFRESH_TIME_VALUE_PREFERENCE = FactsSettings.FACT_REFRESH_TIME_VALUE + "PREFERNECE";

        @Override
        public void onResume() {
            super.onResume();
            registerChangeListener();
        }

        private void registerChangeListener() {
            PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);
        }

        private void unregisterChangeListener() {
            PreferenceManager.getDefaultSharedPreferences(getActivity()).unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            unregisterChangeListener();
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.cat_fact_settings);

            PreferenceScreen preferenceScreen = getPreferenceScreen();


            CheckBoxPreference showNotificationPreference = new CheckBoxPreference(getActivity());
            showNotificationPreference.setTitle("Show notification");
            showNotificationPreference.setKey(FactsSettings.NOTIFICATION_STATUS);
            showNotificationPreference.setChecked(FactsSettings
                    .getShowNotificationStatus(getActivity()));
            showNotificationPreference.setOnPreferenceChangeListener(this);
            showNotificationPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    getActivity().sendBroadcast(FactsSettings.getRefreshIntent(getActivity()));
                    return false;
                }
            });
            preferenceScreen.addPreference(showNotificationPreference);

            CheckBoxPreference showRefreshButtonPreference = new CheckBoxPreference(getActivity());
            showRefreshButtonPreference.setTitle("Show refresh button");
            showRefreshButtonPreference.setSummary("Not recommended due to possibility of running out of facts.");
            showRefreshButtonPreference.setKey(FactsSettings.REFRESH_BUTTON_STATUS);
            showRefreshButtonPreference.setChecked(FactsSettings.getRefreshButtonStatus(getActivity()));
            showRefreshButtonPreference.setOnPreferenceChangeListener(this);
            showRefreshButtonPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    getActivity().sendBroadcast(FactsSettings.getRefreshIntent(getActivity()));
                    return false;
                }
            });
            preferenceScreen.addPreference(showRefreshButtonPreference);

            ListPreference refreshTimePreference = new ListPreference(getActivity());
            refreshTimePreference.setTitle("Refresh time");
            refreshTimePreference.setSummary(FactsSettings.getRefreshTimeForPreferenceSummary(getActivity()));
            refreshTimePreference.setKey(FACT_REFRESH_TIME_VALUE_PREFERENCE);
            refreshTimePreference.setOnPreferenceChangeListener(this);
            refreshTimePreference.setEntryValues(new String[]{"900000", "1800000", "3600000", "14400000", "43200000", "86400000"});
            refreshTimePreference.setEntries(new String[]{"15 minutes", "30 minutes", "1 hour", "4 hours", "12 hours", "24 hours"});
            preferenceScreen.addPreference(refreshTimePreference);

            Preference refreshNow = new Preference(getActivity());
            refreshNow.setTitle("Refresh now");
            refreshNow.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    getActivity().sendBroadcast(FactsSettings.getRefreshIntent(getActivity()));
                    return false;
                }
            });
            preferenceScreen.addPreference(refreshNow);

        }


        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            switch (key) {
                case FactsSettings.NOTIFICATION_STATUS:
                    ((CheckBoxPreference) findPreference(FactsSettings.NOTIFICATION_STATUS)).setChecked(FactsSettings.getShowNotificationStatus(getActivity()));
                    break;
                case FactsSettings.REFRESH_BUTTON_STATUS:
                    ((CheckBoxPreference) findPreference(FactsSettings.REFRESH_BUTTON_STATUS)).setChecked(FactsSettings.getRefreshButtonStatus(getActivity()));

                case FactsSettings.FACT_REFRESH_TIME_VALUE:
                    findPreference(FACT_REFRESH_TIME_VALUE_PREFERENCE).setSummary(FactsSettings.getRefreshTimeForPreferenceSummary(getActivity()));


            }
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            switch (preference.getKey()) {
                case FactsSettings.NOTIFICATION_STATUS:
                    FactsSettings.setNotificationStatus(getActivity(), Boolean.valueOf(newValue.toString()));
                    return false;
                case FactsSettings.REFRESH_BUTTON_STATUS:
                   FactsSettings.setRefreshButtonStatus(getActivity(),Boolean.valueOf(newValue.toString()));
                    return false;
                case FACT_REFRESH_TIME_VALUE_PREFERENCE:
                    FactsSettings.setRefreshTime(getActivity(), Long.valueOf(newValue.toString()));
                    AlarmUtilis.startAlarm(getActivity());
                    return false;

            }
            return true;
        }


    }




}
