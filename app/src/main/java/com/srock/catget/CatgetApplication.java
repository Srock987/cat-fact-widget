package com.srock.catget;

import android.app.Application;
import android.util.Log;

/**
 * Created by Pawel on 2016-01-11.
 */
public class CatgetApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("CAT_FACT", "Application onCreate");
        AlarmUtilis.startAlarm(this);
    }
}
