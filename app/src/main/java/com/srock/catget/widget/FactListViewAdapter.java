package com.srock.catget.widget;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;


import com.srock.catget.R;

import java.util.List;

/**
 * Created by Pawel on 2016-01-04.
 */
public class FactListViewAdapter extends ArrayAdapter<String> {
    private List<String> factList;

    public FactListViewAdapter(Context context, int resource, List<String> factList) {
        super(context, resource, factList);
        this.factList = factList;
    }

    @Override
    public int getCount() {
        return factList.size();
    }

    @Override
    public String getItem(int position) {
        return factList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final String fact=getItem(position);
        ViewHolder holder;

        if (convertView==null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row,parent,false);
            holder.textView = (TextView) convertView.findViewById(R.id.factTextView);
            holder.button = (Button) convertView.findViewById(R.id.share_button);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.textView.setText(fact);
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntetnt = new Intent(Intent.ACTION_SEND);
                sharingIntetnt.setType("text/plain");
                sharingIntetnt.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntetnt.putExtra(Intent.EXTRA_TEXT,fact);
                getContext().startActivity(Intent.createChooser(sharingIntetnt,"Share via:"));
            }
        });
        return convertView;
    }

    private class ViewHolder {
        public TextView textView;
        public Button button;
    }
}
