package com.srock.catget.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.RemoteViews;

import com.srock.catget.FactsSettings;
import com.srock.catget.R;
import com.srock.catget.activities.FactsSettingActivity;
import com.srock.catget.activities.ListActivity;
import com.srock.catget.broadcastreceivers.AlarmBroadcastReceiver;
import com.srock.catget.data.CatFact;
import com.srock.catget.database.DatabaseUtils;

public class CatFactsWidgetProvider extends BroadcastReceiver {

    //Contains current fact that should be shown on widget if enabled
    private CatFact currentCatFact;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(AlarmBroadcastReceiver.CAT_FACT_ARG)) {
            currentCatFact = intent.getParcelableExtra(AlarmBroadcastReceiver.CAT_FACT_ARG);
        } else {
            currentCatFact = DatabaseUtils.getNextCatFact(context);
            DatabaseUtils.setFactShown(context, currentCatFact);
        }
        int[] widgetIds = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, getClass()));
        updateAppWidget(context, AppWidgetManager.getInstance(context), widgetIds, currentCatFact.getFact());
    }

    /**
     * Updates widget with given fact
     *
     * @param context          context
     * @param appWidgetManager AppWidgetManager.
     * @param appWidgetIds      id of widget that we should display fact on.
     * @param fact             String that contains the fact shown on the widget.
     */
    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetIds[], String fact) {

        //Setting up Intent for RemoteAdapter
        Intent viewServiceIntent = new Intent(context, CatFactsRemoteViewsService.class);
        viewServiceIntent.putExtra(CatFactsRemoteViewsService.CAT_FACT_EXTRA, fact);
        viewServiceIntent.setData(Uri.parse(viewServiceIntent.toUri(Intent.URI_INTENT_SCHEME)));

        //which layout to show on widget
        RemoteViews widget = new RemoteViews(context.getPackageName(),
                R.layout.catfacts_widget);

        //setting adapter to listView of the widget
        widget.setRemoteAdapter(R.id.factListView, viewServiceIntent);

        //variable for visibility status of refresh button on widget
        int refreshButtonVisibility;
        //checking if refresh button should be visible or gone
        if (FactsSettings.getRefreshButtonStatus(context)) {
            refreshButtonVisibility = View.VISIBLE;
        } else {
            refreshButtonVisibility = View.GONE;
        }
        //setting visibilty of refresh button
        widget.setViewVisibility(R.id.refresh_button, refreshButtonVisibility);

        // creating intent for settings button
        Intent intentSettings = new Intent(context, FactsSettingActivity.class);
        // creating intent for listView onClick
        Intent shareActivityIntent = new Intent(context, ListActivity.class);
        // creating intent for share button
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);

        //specifying sharingIntent
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, fact);

        //creating pendingIntents for settings button and listView onClick to start activities
        PendingIntent pendingSettingsIntent = PendingIntent.getActivity(context, 0, intentSettings, 0);
        PendingIntent shareActivityPendingIntent = PendingIntent.getActivity(context, 0, shareActivityIntent, 0);

        //creating pendingIntent for share button to start sharing options
        PendingIntent pendingShareIntent = PendingIntent.getActivity(context, 0, Intent.createChooser(sharingIntent, "Share via:"), PendingIntent.FLAG_UPDATE_CURRENT);

        //creating pendingIntent for refresh button to update widget
        PendingIntent pendingRefreshIntent = PendingIntent.getBroadcast(context, 0, FactsSettings.getRefreshIntent(context), PendingIntent.FLAG_UPDATE_CURRENT);

        // getting layout for widget and attaching it to on-click listeners for the buttons
        widget.setOnClickPendingIntent(R.id.refresh_button, pendingRefreshIntent);
        widget.setOnClickPendingIntent(R.id.settings_button, pendingSettingsIntent);
        widget.setOnClickPendingIntent(R.id.share_button, pendingShareIntent);

        //getting layout for listView and attaching it to PendingIntentTemplate
        widget.setPendingIntentTemplate(R.id.factListView, shareActivityPendingIntent);

        //updating widget
        appWidgetManager.updateAppWidget(appWidgetIds, widget);
    }
}