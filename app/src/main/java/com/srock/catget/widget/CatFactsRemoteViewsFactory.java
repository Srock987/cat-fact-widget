package com.srock.catget.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.srock.catget.R;
import com.srock.catget.activities.ListActivity;

import java.util.List;

public class CatFactsRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private final Context context;
    private final int appWidgetId;
    private List<String> catfacts;


    public CatFactsRemoteViewsFactory(Context context, Intent intent, List<String> catfacts) {
        this.catfacts = catfacts;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
        this.context = context;
    }

    @Override
    public void onCreate() {
        // no-op
    }

    @Override
    public void onDestroy() {
        // no-op
    }

    @Override
    public int getCount() {
        return (catfacts.size());
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews row = new RemoteViews(context.getPackageName(),
                R.layout.widget_list_row);
        row.setTextViewText(R.id.widgetRowTextView, catfacts.get(position));
        Intent shareActivityIntent = new Intent(context, ListActivity.class);
        row.setOnClickFillInIntent(R.id.widgetRowTextView,shareActivityIntent);
        return (row);
    }

    @Override
    public RemoteViews getLoadingView() {
        return (null);
    }

    @Override
    public int getViewTypeCount() {
        return (1);
    }

    @Override
    public long getItemId(int position) {
        return (position);
    }

    @Override
    public boolean hasStableIds() {
        return (true);
    }

    @Override
    public void onDataSetChanged() {
        // no-op
    }
}
