package com.srock.catget.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

import java.util.Arrays;

/**
 * Created by Pawel on 2015-11-13.
 */
public class CatFactsRemoteViewsService extends RemoteViewsService {

    public static final String CAT_FACT_EXTRA = "cat_fact";

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        String catFact = intent.getStringExtra(CAT_FACT_EXTRA);
        return (new CatFactsRemoteViewsFactory(getApplicationContext(), intent, Arrays.asList(new String[]{catFact})));
    }
}
