package com.srock.catget.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Pawel on 2016-01-11.
 */
public class BootBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        /*Nothing here, the point is to call onCreate() method
         of Application class where alarm manager is started.
         */
    }
}
