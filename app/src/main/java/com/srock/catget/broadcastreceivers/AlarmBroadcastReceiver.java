package com.srock.catget.broadcastreceivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.srock.catget.FactsSettings;
import com.srock.catget.R;
import com.srock.catget.widget.CatFactsWidgetProvider;
import com.srock.catget.activities.ListActivity;
import com.srock.catget.data.CatFact;
import com.srock.catget.database.DatabaseUtils;

/**
 * Created by Pawel on 2016-01-07.
 */
public class AlarmBroadcastReceiver extends BroadcastReceiver {

    public static final String CAT_FACT_ARG = "catFactArgument";
    public static final String NOTIFICATION_CONTENT_TITLE = "Did you know?";
    public static final int NOTIFICATION_ID = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        CatFact catFact = DatabaseUtils.getNextCatFact(context);
        if (FactsSettings.getShowNotificationStatus(context)) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(NOTIFICATION_ID, getNotification(context, catFact));
        }
        sendWidgetBroadcast(context, catFact);
        DatabaseUtils.setFactShown(context, catFact);
    }

    /**
     * This method sends the broadcast
     * @param context context
     * @param catFact parcelable object that contains string factID and string fact.
     */
    private void sendWidgetBroadcast(Context context, CatFact catFact) {
        Intent intent = new Intent(context, CatFactsWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = {1234567};
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        intent.putExtra(AlarmBroadcastReceiver.CAT_FACT_ARG, catFact);
        context.sendBroadcast(intent);
    }

    /**
     * This method creates notification with intent that starts
     * ListActivity.class, gives notification title, text,
     * icon, and flag closing notification after click.
     * @param context
     * @param fact String that contains the fact shown on the widget
     * @return this method returns notification
     */
    private static Notification getNotification(Context context, CatFact fact) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        PendingIntent myIntent = PendingIntent.getActivity(context, 0, new Intent(context, ListActivity.class), 0);
        builder.setContentIntent(myIntent);
        builder.setContentTitle(NOTIFICATION_CONTENT_TITLE);
        builder.setContentText(fact.getFact());
        builder.setSmallIcon(R.drawable.ic_stat_ic_notification_icon);
        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        return notification;
    }

}
