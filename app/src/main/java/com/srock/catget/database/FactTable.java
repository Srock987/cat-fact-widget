package com.srock.catget.database;

/**
 * Created by Pawel on 2015-11-02.
 */
public class FactTable {
    public static final String TABLE_NAME = "cat_facts";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_FACT = "fact";
    public static final String COLUMN_FACT_SHOWN = "shown";
    public static final String COLUMN_SHOW_ORDER = "show_order";

}
