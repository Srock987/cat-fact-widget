package com.srock.catget.database;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by Pawel on 2015-10-30.
 */
public class FactsSQLOpenHelper extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "cats_facts.db";
    private static final int DATABASE_VERSION = 1;

    public FactsSQLOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    }
