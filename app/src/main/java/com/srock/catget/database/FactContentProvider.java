package com.srock.catget.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.content.UriMatcher;
import android.text.TextUtils;

/**
 * Created by Pawel on 2015-11-02.
 */
public class FactContentProvider extends ContentProvider {

    // used for the UriMacher
    private static final int CATFACTS = 10;
    private static final int CATFACT_ID = 20;

    private static final String AUTHORITY = "com.srock.catget";

    private static final String BASE_PATH = "widget";

    private FactsSQLOpenHelper database;

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/facts";

    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/fact";

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, CATFACTS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", CATFACT_ID);
    }

    @Override
    public boolean onCreate() {
        database = new FactsSQLOpenHelper(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        // Uisng SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();


        // Set the table
        queryBuilder.setTables(FactTable.TABLE_NAME);

        int uriType = sURIMatcher.match(uri);

        switch (uriType) {
            case CATFACTS:
                break;
            case CATFACT_ID:
                // adding the ID to the original query
                queryBuilder.appendWhere(FactTable.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase db = database.getWritableDatabase();
        String id;
        int rowsUpdated;
        switch (uriType) {
            case CATFACT_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = db.update(FactTable.TABLE_NAME, values, FactTable.COLUMN_ID + "=" + id, null);
                } else {
                    rowsUpdated = db.update(FactTable.TABLE_NAME, values, FactTable.COLUMN_ID + "=" + id + "and" + selection, selectionArgs);
                }
                break;
            case CATFACTS:
                rowsUpdated = db.update(FactTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

}
