package com.srock.catget.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

import com.srock.catget.data.CatFact;

/**
 * Created by Pawel on 2016-01-11.
 */
public class DatabaseUtils {

    private static final int DEFAULT_SHOW_ORDER = 1;

    /**
     * This method get the sql database in random order
     * with selection FactTable.COLUMN_FACT_SHOWN=0(fact not shown yet)
     * If all the facts has been shown it randoms out of all facts.
     * Then returns catFact that is provided by getFirstCatFact().
     *
     * @param context context
     * @return
     */
    public static CatFact getNextCatFact(Context context) {
        Cursor cursor = context.getContentResolver().query(FactContentProvider.CONTENT_URI, null,
                FactTable.COLUMN_FACT_SHOWN + "=?", new String[]{"0"}, "RANDOM() LIMIT 1");
        CatFact catFact = getFirstCatFact(cursor);
        if (catFact == null) {
            cursor = context.getContentResolver().query(FactContentProvider.CONTENT_URI, null,
                    null, null, "RANDOM() LIMIT 1");
            catFact = getFirstCatFact(cursor);
        }
        return catFact;
    }

    /**
     * This method updates sql database after the fact has been displayed
     * and in column FactTable.COLUMN_FACT_SHOWN marks it as shown(=1)
     * also writes in column FactTable.COLUMN_SHOW_ORDER number depending
     * on the return of the checkHighestShowOrder().
     *
     * @param context context
     * @param catFact parcelable object that contains string factID and string fact.
     */
    public static void setFactShown(Context context, CatFact catFact) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FactTable.COLUMN_FACT_SHOWN, 1);
        contentValues.put(FactTable.COLUMN_SHOW_ORDER, checkHighestShowOrder(context));
        context.getContentResolver().update(Uri.withAppendedPath(FactContentProvider.CONTENT_URI, catFact.getId()), contentValues, null, null);

    }

    /**
     * This method set cursor to sort database by the highest value
     * in the FactTable.COLUMN_SHOW_ORDER get the value if the there is no value
     * in any row returns 1 and if there is increment it by 1 then returns it
     *
     * @param context context
     * @return 1 or highest value in FactTable.COLUMN_SHOW_ORDER incremented by 1
     */
    private static int checkHighestShowOrder(Context context) {
        Cursor cursor = context.getContentResolver().query(FactContentProvider.CONTENT_URI, null, null, null, FactTable.COLUMN_SHOW_ORDER + " DESC");
        if (cursor != null && cursor.moveToFirst()) {
            String highestOrder = cursor.getString(cursor.getColumnIndex(FactTable.COLUMN_SHOW_ORDER));
            if (TextUtils.isEmpty(highestOrder)) {
                return DEFAULT_SHOW_ORDER;
            } else {
                return Integer.parseInt(highestOrder) + 1;
            }
        }
        return DEFAULT_SHOW_ORDER;
    }

    /**
     * This method get strings catFactText and catFactId from sql database
     * and package them into object of the CatFact class and return that object.
     *
     * @param cursor
     * @return
     */
    private static CatFact getFirstCatFact(Cursor cursor) {
        if (cursor != null && cursor.moveToFirst()) {
            String catFactText = cursor.getString(cursor.getColumnIndex(FactTable.COLUMN_FACT));
            String catFactId = cursor.getString(cursor.getColumnIndex(FactTable.COLUMN_ID));
            return new CatFact(catFactText, catFactId);
        }
        return null;
    }
}
